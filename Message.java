package ex;
import java.util.Scanner;
public class Message{
    public static void main(String[] args) throws LongerThenTenException{
        Scanner input = new Scanner(System.in);
        String [] message = input.nextLine().split("\\s+");
        for(String word:message) {
            try {
                printWord(word);
            }
            catch (LongerThenTenException e){
                System.out.println("Слово не может быть выведено (смотреть документацию LongerThenTenException)");
            }
        }
    }

    public static void printWord(String word) throws LongerThenTenException{
        if (word.length() > 10){
            throw new LongerThenTenException();
        }else {
            System.out.println(word);
        }
    }
}
